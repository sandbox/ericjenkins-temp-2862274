/**
@file
Contains \Drupal\test_project_2\Controller\MyController.
 */

namespace Drupal\test_project_2\Controller;

use Drupal\Core\Controller\ControllerBase;

class MyController extends ControllerBase {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello world'),
    );
  }
}
